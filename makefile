all: install


uninstall:
	@echo "uninstalling from /usr/local/bin/ ..."
	@sudo rm -i /usr/local/bin/.update /usr/local/bin/update

install:
	@echo "installing to /usr/local/bin/ ..."
	@sudo cp -i .update update /usr/local/bin/
	@sudo chmod ugo+x /usr/local/bin/update
	@sudo chmod ugo+r /usr/local/bin/.update

